   
   
## categories
   
    Digital electronics
    Analogue electronics
    Microelectronics
    Circuit design
    Integrated circuits
    Power electronics
    Optoelectronics
    Semiconductor devices
    Embedded systems
    Audio electronics
    Telecommunications
    Nanoelectronics
    Bioelectronics

### components

https://passive-components.eu/knowledge-blog/

Capacitors

What is a Capacitor ? 

Capacitor Symbols 

Capacitance and its Calculation, Dielectric, Dipoles and Dielectric Absorption 

Dielectric Constant and its Effects on the Properties of a Capacitor 

Leakage Current Characteristics of Capacitors 

Insulation Resistance, DCL Leakage Current and Breakdown Voltage 

Capacitor Losses (ESR, IMP, DF, Q), Series or Parallel Eq. Circuit ? 

ESR Characteristics of Capacitors 

Effects of ESL on Capacitor Performance 

Capacitor Energy Content and Force 

Capacitors Derating and Category Concepts 

Capacitor Ripple Current, Transient and Surge Power Load Ratings 

Ripple Current and its Effects on the Performance of Capacitors 

Capacitor Technologies Overview 

Construction of Electrostatic Capacitors 

MLCC and Ceramic Capacitors 

Film and Foil Organic Dielectric Capacitors 

Silicon and Silicon Wafer Based Integrated Capacitors 

Glass, MICA, Air and Vacuum Capacitors 

Electrolytic Capacitors 

Aluminum Electrolytic Capacitors 

Tantalum and Niobium Capacitors 

Supercapacitors 

Variable Capacitors and Trimmers 



### Circuit design


#### electronic-system

https://www.electronics-tutorials.ws/systems/electronic-system.html

feedback systems

In feedback systems, a fraction of the output signal is “fed back” and either added to or subtracted from the original input signal. The result is that the output of the system is continually altering or updating its input with the purpose of modifying the response of a system to improve stability. A feedback system is also commonly referred to as a “Closed-loop System” as shown.

Feedback systems are used a lot in most practical electronic system designs to help stabilise the system and to increase its control. If the feedback loop reduces the value of the original signal, the feedback loop is known as “negative feedback”. If the feedback loop adds to the value of the original signal, the feedback loop is known as “positive feedback”.

An example of a simple feedback system could be a thermostatically controlled heating system in the home. If the home is too hot, the feedback loop will switch “OFF” the heating system to make it cooler. If the home is too cold, the feedback loop will switch “ON” the heating system to make it warmer. In this instance, the system comprises of the heating system, the air temperature and the thermostatically controlled feedback loop.